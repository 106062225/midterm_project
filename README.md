# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : [Hwitter呼特]
* Key functions (add/delete)
    1. user page
    2. post page
    3. post list page
    4. leave comment under any post
    
* Other functions (add/delete)
    1. upload your photo sticker on user page
    2. see poeple's hwit on their user page (click their name)
    3. hwitter flying on topbar

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description

## 作品網址

    https://midterm-project-1075b.firebaseapp.com/index.html

## Components Description

1. user page: in your user page, you can see your all hwit and you can change your photo sticker.
2. post page: hwit and it will list on the current board that you stay on, also the time is recorded and shown beside the post. max word for post are setted.
3. post list page: see hwits by clicking different board, also you can click Home to see all hwits. click any post to see the content and you are able to comment. click user name to see their information including their user name, photo sticker and all their hwits. loading gif displayed during loading hwits.
4. leave comment under any post: click any post on post list page and you can leave your comment. try comment button and it will bring you to the bottom of the page so you can type your comment. comment time is recorded and show beside comment.
5. login page with log in and creat account.
6. sign in with google account is allowed.
7. CSS animation: lovely hwitter is flying on the sky. hover on google icon trigger a notification block.
8. topbar change when user log in, still the hwitter flying without having any breaktime.
9. RWD: viewing website with smartphone is satisfied.

## Other Functions Description(1~10%)

1. upload your own photo sticker is allowed.
2. view the user's all hwit on their home page.
3. auto scroll when click comment button.
4. beautiful and twitter-similar style.

## Security Report (Optional)

1. realtime database rule, sign in to read/write
2. leaving html tag on post or comment is regard as letter, meaning that it won't break the web structure.