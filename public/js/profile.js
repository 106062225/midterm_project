function init() {
    var photoUpload = document.getElementById('photoUpload');

    firebase.auth().onAuthStateChanged(user => {
        if (user) {
            if(user.uid === location.search.split("?id=")[1]){
                photoUpload.style.display = 'inline';
            }else{
                photoUpload.style.display = 'none';
            }
        } else {
            photoUpload.style.display = 'none';
        }
    });
    
    firebase.database().ref('/users/' + location.search.split("?id=")[1]).once('value').then(function(snapshot) {
        document.getElementById('card_img').setAttribute('src', snapshot.val().photoURL);
        document.getElementById('card_container').innerHTML = "<h4><b>" + snapshot.val().displayName + "</b></h4>";
        var startPostTime;
        var database = firebase.database().ref('/posts').orderByChild('gTime').limitToFirst(displayPostStart);
        database.once('value', function(snapshotin){
            snapshotin.forEach(function(childSnapshot) {
                displayPostStart--;
                startPostTime = childSnapshot.val().gTime;
            });
            if(displayPostStart <= 0){
                database = firebase.database().ref('/posts').orderByChild('gTime').startAt(startPostTime).limitToFirst(15);
                database.once('value', function(snapshot2){
                    $('ul#post_list2').html("");
                    snapshot2.forEach(function(childSnapshot) {
                        if(childSnapshot.val().authorID==snapshot.val().uid){
                            var plist = "<li class='list-group-item' >" + "<a class='post_link' id='" + childSnapshot.key + "' href='post.html?id="+ childSnapshot.key + "' style='text-decoration:none;'>" + "</a>" + "<small style='float: right;margin-top:2px;'>" + childSnapshot.val().month+ "/" + childSnapshot.val().day + " " + childSnapshot.val().hours + ":" + childSnapshot.val().minutes + "</small>" + "<a id='author_link' href='profile.html?id=" + childSnapshot.val().authorID + "'>" + "<small style='float: right;margin-top:2px;margin-right:13px'>" + childSnapshot.val().author + "</small>"+ "</a>" + '</li>';
                            $('ul#post_list2').append(plist);      
                            $('#' + childSnapshot.key).text(childSnapshot.val().title);
                        }
                    });

                })
            }else{
                $('ul#post_list2').html("<li class='list-group-item loading' style='text-align: center;'>去呼一下吧</li>");
            }
        })
    });
    
    var storageRef = firebase.storage().ref();
    var uploadFileInput = document.getElementById("uploadFileInput");
    uploadFileInput.addEventListener("change", function(){
        var file = this.files[0];
        var user = firebase.auth().currentUser;
        var uploadTask = storageRef.child('userPhotos/' + user.uid + "/" + file.name).put(file);
        uploadTask.on('state_changed', function(snapshot){
            var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            console.log('Upload is ' + progress + '% done');
            switch (snapshot.state) {
                case firebase.storage.TaskState.PAUSED:
                    console.log('Upload is paused');
                    break;
                case firebase.storage.TaskState.RUNNING:

                    console.log('Upload is running');
                    break;
            }
        }, function(e) {

        }, function() {
            var downloadURL = uploadTask.snapshot.downloadURL;
            console.log(downloadURL);
            user.updateProfile({
                photoURL: downloadURL
            }).then(function(){
                var updates = {};
                updates['users/' + user.uid + "/photoURL"] = user.photoURL;
                firebase.database().ref().update(updates).then(function() {
                    location.reload();
                }).catch(function(error){
                    console.log(error);
                });
            }, function(error) {
                console.log(error);
            });
        });
    },false);
    $('#proall').click(function(){
        firebase.database().ref('/board').set({
            nextboard: 'all',
        })
        location = location;
    })
    var currentPage = location.search.split("?page=")[1];
    if(!currentPage){
        currentPage = 1; 
    }

    var preP = (parseInt(currentPage)==1)?1:(parseInt(currentPage) - 1);
    var nxtP = parseInt(currentPage)+1;
    var p = '<li class="page-item"><a class="page-link" href="?page=' + preP + '" aria-label="Previous"><span aria-hidden="true">&lsaquo;</span><span class="sr-only">Previous</span></a></li>';
    var n = '<li class="page-item"><a class="page-link" href="?page=' + nxtP + '" aria-label="Next"><span aria-hidden="true">&rsaquo;</span><span class="sr-only">Next</span></a></li>';
    var newPagination = p;

    var i;
    for(i = parseInt(currentPage) - 2; i <= parseInt(currentPage) + 2; i++){
        if(i > 0){
            newPagination += '<li class="page-item';
            if(i == currentPage){
                newPagination += ' active"><a class="page-link" href="#">';
            }else{
                newPagination += '"><a class="page-link" href="?page=' + i + '">';
            }
            newPagination += i + '</a></li>';
        }
    }

    newPagination += n;

    $('#thepagination2').append(newPagination);
    var displayPostStart = (parseInt(currentPage) - 1)*15 + 1;
    
}

window.onload = function () {
    init();
    initTopNav();    
};
