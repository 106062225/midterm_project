function init() {
     // Login with Email/Password
    var txtEmail1 = document.getElementById('txtEmail1');
    var txtPassword1 = document.getElementById('txtPassword1');

    var txtEmail2 = document.getElementById('txtEmail2');
    var txtPassword2 = document.getElementById('txtPassword2');

    var btnLogin = document.getElementById('btnLogin');
    var btnCreate = document.getElementById('btnCreate');

    btnLogin.addEventListener('click', function(){
        var email = txtEmail1.value;
        var password = txtPassword1.value;
        firebase.auth().signInWithEmailAndPassword(email, password)
        .then(function(user) {
            location.href="index.html";
        })
        .catch(function(e){ 
            var login_error = document.getElementById('login_error');
            login_error.innerHTML = e.message;
        });
    });

    btnCreate.addEventListener('click', function(){
        var email = txtEmail2.value;
        var password = txtPassword2.value;
        firebase.auth().createUserWithEmailAndPassword(email, password)
        .then(function(user) {
            user.updateProfile({
                displayName: document.getElementById("userName").value,
                photoURL: "https://i.imgur.com/9DhTfKE.png"
            })
            .then(function () {
                firebase.database().ref('users/' + user.uid)
                .set({
                    uid: user.uid,
                    displayName: user.displayName,
                    email: user.email,
                    photoURL: user.photoURL,
                })
                .then(function() {
                    location.href="index.html";
                })
                .catch(function(error){
                    console.error("寫入使用者資訊錯誤",error);
                });
            }, function (e) {
                console.log("Error happened");
            });
        })
        .catch(function(e){ 
            var signup_error = document.getElementById('signup_error');
            signup_error.innerHTML = e.message;
            console.log(e.message);
        });
    });

    function Logout(){
        firebase.auth().signOut();
    }

    // // Login with Google
    var btnLoginGooglePop = document.getElementById('btnLoginGooglePop');

    var provider = new firebase.auth.GoogleAuthProvider();

    btnLoginGooglePop.addEventListener('click', e => {
        console.log('signInWithPopup');
        firebase.auth().signInWithPopup(provider).then(function (result) {
            var token = result.credential.accessToken;
            var user = result.user;
            firebase.database().ref('users/' + user.uid).set({
                uid: user.uid,
                displayName: user.displayName,
                email: user.email,
                photoURL: user.photoURL,
            }).then(function() {
                location.href="index.html";
            }).catch(function(error){
                console.error("寫入使用者資訊錯誤",error);
            });
        }).catch(function (error) {
            login_error.innerHTML = error.message;
            console.log('error: ' + error.message);
        });
    });

    var logout_link = document.getElementById('logout_link');
    var login_link = document.getElementById('login_link');

    firebase.auth().onAuthStateChanged(user => {
        if (user) {
            // console.log(user);
        } else {
            console.log('not logged in');
        }
    });


    $('#signin').submit(function () {
        return false;
    });

    $('#signup').submit(function () {
        return false;
    });
}

window.onload = function () {
    init(); 
    initTopNav();
};