function initTopNav() {
    var userpage_link = document.getElementById('userpage_link');
    var user_img = document.getElementById('user_img');
    var logout_link = document.getElementById('logout_link');
    var login_link = document.getElementById('login_link');
    var logout_link = document.getElementById('logout_link');

    firebase.auth().onAuthStateChanged(user => {
        if (user) {
            user_img.style.display = 'block';
            user_img.style.backgroundImage = 'url(' + user.photoURL +')';
            userpage_link.style.display = 'block';
            userpage_link.innerHTML = user.displayName;
            userpage_link.setAttribute('href', 'profile.html?id=' + user.uid);
            logout_link.style.display = 'inline';
            logout_link.style.color = 'white';
            login_link.style.display = 'none';
            
        } else {
            user_img.style.display = 'none';
            userpage_link.style.display = 'none';
            logout_link.style.display = 'none';
            login_link.style.display = 'inline';
        }
    });

    logout_link.addEventListener('click', function(){
        firebase.auth().signOut()
        .then(function(){
            window.location.href="index.html";
        }).catch(e => alert(e.messege));
    });
}