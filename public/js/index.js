function init() {
    $('#post_btn').click(function(){
        var user = firebase.auth().currentUser;
        if(!user){
            alert("please sign in first");
            location.href='signin.html';
        }
    })
    $('#post_btn').on('shown.bs.modal', function () {
        $('#exampleModal').trigger('focus');
    })
    

    $('#new_post_form').submit(function(event){
        var user = firebase.auth().currentUser;
        var postRef = firebase.database().ref('/posts');
        var NowDate = new Date();
        postRef.push().set({
            author: user.displayName,
            authorID: user.uid,
            authorPhoto: user.photoURL,
            title: $('input:first').val(),
            content: $('textarea').val(),
            month: (NowDate.getMonth()<10)?('0'+NowDate.getMonth()):(NowDate.getMonth()),
            day: (NowDate.getDate()<10)?('0'+NowDate.getDate()):(NowDate.getDate()),
            hours: (NowDate.getHours()<10)?('0'+NowDate.getHours()):(NowDate.getHours()),
            minutes: (NowDate.getMinutes()<10)?('0'+NowDate.getMinutes()):(NowDate.getMinutes()),
            gTime: -NowDate.getTime(),
            board: cboard,
        }).then(function(){
            location.href="index.html";
        }).catch(function(e){
        })
        return false;
    })

    
    var currentPage = location.search.split("?page=")[1];
    if(!currentPage){
        currentPage = 1; 
    }

    var preP = (parseInt(currentPage)==1)?1:(parseInt(currentPage) - 1);
    var nxtP = parseInt(currentPage)+1;
    var p = '<li class="page-item"><a class="page-link" href="?page=' + preP + '" aria-label="Previous"><span aria-hidden="true">&lsaquo;</span><span class="sr-only">Previous</span></a></li>';
    var n = '<li class="page-item"><a class="page-link" href="?page=' + nxtP + '" aria-label="Next"><span aria-hidden="true">&rsaquo;</span><span class="sr-only">Next</span></a></li>';
    var newPagination = p;

    var i;
    for(i = parseInt(currentPage) - 2; i <= parseInt(currentPage) + 2; i++){
        if(i > 0){
            newPagination += '<li class="page-item';
            if(i == currentPage){
                newPagination += ' active"><a class="page-link" href="#">';
            }else{
                newPagination += '"><a class="page-link" href="?page=' + i + '">';
            }
            newPagination += i + '</a></li>';
        }
    }

    newPagination += n;

    $('#thepagination').append(newPagination);

    var cd = firebase.database().ref('/board');
    var cboard = 'all';
    cd.on('value', function (snapshot) {
        cboard = snapshot.val().nextboard;
        $('#DEtitle').append("<a style='float:right'>"+"-"+cboard+"</a>");
    });
    var displayPostStart = (parseInt(currentPage) - 1)*15 + 1;
    var startPostTime;
    var database = firebase.database().ref('/posts').orderByChild('gTime').limitToFirst(displayPostStart);
    database.once('value', function(snapshot){
        snapshot.forEach(function(childSnapshot) {
            displayPostStart--;
            startPostTime = childSnapshot.val().gTime;
        });
        if(displayPostStart <= 0){
            database = firebase.database().ref('/posts').orderByChild('gTime').startAt(startPostTime).limitToFirst(15);
            database.once('value', function(snapshot){
                $('ul#post_list').html("");
                snapshot.forEach(function(childSnapshot) {
                    if(childSnapshot.val().board==cboard){
                        var plist = "<li class='list-group-item' >" + "<a class='post_link' id='" + childSnapshot.key + "' href='post.html?id="+ childSnapshot.key + "' style='text-decoration:none;'>" + "</a>" + "<small style='float: right;margin-top:2px;'>" + childSnapshot.val().month+ "/" + childSnapshot.val().day + " " + childSnapshot.val().hours + ":" + childSnapshot.val().minutes + "</small>" + "<a id='author_link' href='profile.html?id=" + childSnapshot.val().authorID + "'>" + "<small style='float: right;margin-top:2px;margin-right:13px'>" + childSnapshot.val().author + "</small>"+ "</a>" + '</li>';
                        $('ul#post_list').append(plist);      
                        $('#' + childSnapshot.key).text(childSnapshot.val().title);
                    }
                    else if(cboard=='all'){
                        var plist = "<li class='list-group-item' >" + "<a class='post_link' id='" + childSnapshot.key + "' href='post.html?id="+ childSnapshot.key + "' style='text-decoration:none;'>" + "</a>" + "<small style='float: right;margin-top:2px;'>" + childSnapshot.val().month+ "/" + childSnapshot.val().day + " " + childSnapshot.val().hours + ":" + childSnapshot.val().minutes + "</small>" + "<a id='author_link' href='profile.html?id=" + childSnapshot.val().authorID + "'>" + "<small style='float: right;margin-top:2px;margin-right:13px'>" + childSnapshot.val().author + "</small>"+ "</a>" + '</li>';
                        $('ul#post_list').append(plist);      
                        $('#' + childSnapshot.key).text(childSnapshot.val().title);
                    }
                });

            })
        }else{
            $('ul#post_list').html("<li class='list-group-item loading' style='text-align: center;'>沒人在呼</li>");
        }
    })
    $('#inall').click(function(){
        firebase.database().ref('/board').set({
            nextboard: 'all',
        })
        location = location;
    })
}
window.onload = function () {
    init();
    initTopNav();    
};
function changeboard(x){
    firebase.database().ref('/board').set({
        nextboard: x,
    })
    location = location;
}