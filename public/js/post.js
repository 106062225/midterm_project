function init() {
    firebase.database().ref('/posts/' + location.search.split("?id=")[1]).once('value').then(function(snapshot) {
        document.getElementById('post_list_nav').innerHTML = '<div id="authorImg" style="width:66px;height:66px;float:left;border:0px solid gray;background-size: cover;background-position: center; margin-left: 15px;"></div><h4 style="margin-top:19px;margin-left:20px;float:left;">' + snapshot.val().author + '</h4>';
        document.getElementById('post_list_nav').style.height = "100px";
        firebase.database().ref('/users/' + snapshot.val().authorID).once('value').then(function(_snapshot) {
            document.getElementById('authorImg').style.backgroundImage = 'url("' + _snapshot.val().photoURL + '")';
        });
        document.getElementById('content').innerHTML ="<h3>" + "<div id='title' style='float: left;'></div>" + "</h3>" + "<small style='float: right;margin-top:3px;color:#bdbdbd;'>" + snapshot.val().month+ "/" + snapshot.val().day + " " + snapshot.val().hours + ":" + snapshot.val().minutes + "</small>" + "<div id='DEcontent' style='white-space: pre-wrap;float: left;width:100%;padding-top:20px;padding-bottom:50px;'>" + "</div>" + "<div id='comments' style='white-space: pre-wrap;float: left;width:100%;padding-top:20px;padding-bottom:50px;'>" + "</div>";
        $('#title').text(snapshot.val().title);
        $('#DEcontent').text(snapshot.val().content);

        var commentHr = '<h4>Comments</h4><button id="btnscroll" type="button" class="btn btn-outline-secondary btn-sm" style="float:right;margin-top:-36px;">Comment</button>';
        $('div#comments').append(commentHr);

        var user = firebase.auth().currentUser;

        if(user){
            var commentForm = '<form id="comment_form" ><div class="form-group"><textarea class="form-control" id="new_post_content" rows="5" required="required"  wrap="Virtual" placeholder="type here" maxlength="100"></textarea></div><button type="submit" class="btn btn-secondary"style="float: right;margin-left:10px">Send</button></form>';
            $('div#comment').append(commentForm);
        }else{
            $('div#comment').append('Sign in to leave comments.');
        }
        
        var first_count = 0, second_count = 0;

        var database = firebase.database().ref('/posts/' + location.search.split("?id=")[1] + "/comments").orderByChild('gTime');
        database.once('value', function(snapshot){
            snapshot.forEach(function(childSnapshot) {
                first_count++;
                var comments = '<hr>';
                comments += '<div id="commentAuthorImg" style="background-image: url(' + "'" + childSnapshot.val().authorPhoto + "'" + ');width:50px;height:50px;float:left;border-radius:25px;border:1px solid gray;background-size: cover;background-position: center;"></div>'
                comments += "<small style='float: right;margin-top:3px;color:#bdbdbd;'>" + childSnapshot.val().month + "/" + childSnapshot.val().day + "-" + childSnapshot.val().hours + ":" + childSnapshot.val().minutes + "</small>";
                comments += '<h6 style="margin-top:30px;margin-left:60px;margin-bottom:40px;">' + childSnapshot.val().author + '</h6>'
                comments += '<div id="' + childSnapshot.key + '" style="margin-left:7px;white-space: pre-wrap;"></div>';
                $('div#comments').append(comments);
                $('#' + childSnapshot.key).text(childSnapshot.val().content);
            });

            database.on('child_added', function(childSnapshot) {
                second_count++;
                if(second_count > first_count){
                    var comments = '<hr>';
                    var NowDate = new Date();
                    comments += '<div id="commentAuthorImg" style="background-image: url(' + "'" + childSnapshot.val().authorPhoto + "'" + ');width:50px;height:50px;float:left;border-radius:25px;border:1px solid gray;background-size: cover;background-position: center;"></div>'
                    comments += "<small style='float: right;margin-top:3px;color:#bdbdbd;'>" + childSnapshot.val().month + "/" + childSnapshot.val().day + "-" + childSnapshot.val().hours + ":" + childSnapshot.val().minutes + "</small>";
                    comments += '<h6 style="margin-top:30px;margin-left:60px;margin-bottom:40px;">' + childSnapshot.val().author + '</h6>';
                    comments += '<div id="' + childSnapshot.key + '" style="margin-left:7px;white-space: pre-wrap;"></div>';
                    $('div#comments').append(comments);
                    $('#' + childSnapshot.key).text(childSnapshot.val().content);
                    //roll when new comment
                    $('html, body').scrollTop($(document).height());
                }
            });

        })

        $('#comment_form').submit(function(event){
            var user = firebase.auth().currentUser;
            var commentRef = firebase.database().ref('/posts/' + location.search.split("?id=")[1] + "/comments");
            var NowDate = new Date();
            commentRef.push().set({
                author: user.displayName,
                authorID: user.uid,
                authorPhoto: user.photoURL,
                content: $('textarea').val(),
                gTime: NowDate.getTime(),
                month: (NowDate.getMonth()<10)?('0'+NowDate.getMonth()):(NowDate.getMonth()),
                day: (NowDate.getDate()<10)?('0'+NowDate.getDate()):(NowDate.getDate()),
                hours: (NowDate.getHours()<10)?('0'+NowDate.getHours()):(NowDate.getHours()),
                minutes: (NowDate.getMinutes()<10)?('0'+NowDate.getMinutes()):(NowDate.getMinutes()),
            })
            .then(function(){
                $('textarea').val('');
            })
            .catch(function(e){
            })
            return false;
        })

        $('#btnscroll').click(function(){
            $("html, body").animate({ scrollTop: $(document).height() }, 500);
        })
    });
    $('#postall').click(function(){
        firebase.database().ref('/board').set({
            nextboard: 'all',
        })
        location = location;
    })
}

window.onload = function () {
    init();
    initTopNav();
};
